### What is this repository for? ###

This repository contains examples of using undocumented and public API for Virtual Desktops in Windows 10.

### Some info ###

Last tested version of Windows 10: **build 10558**.

Visual Studio 2015 Community Edition used for developing.